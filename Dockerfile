# Use the official Nginx base image
FROM nginx:latest

# Copy the custom Nginx configuration file if needed
# COPY nginx.conf /etc/nginx/nginx.conf

# Copy the HTML file into the Nginx default public directory
COPY index.html /usr/share/nginx/html/

# Expose the default Nginx port
EXPOSE 80

# Command to start Nginx when the container starts
CMD ["nginx", "-g", "daemon off;"]

